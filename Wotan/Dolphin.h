#ifndef DOLPHIN_H
#define DOLPHIN_H

#include "Mammal.h"

class Dolphin : public Mammal
{
public:
    Dolphin(int breasts, int fins);
    ~Dolphin();
    void swim();

private:
    int m_fins;
};

#endif // DOLPHIN_H
