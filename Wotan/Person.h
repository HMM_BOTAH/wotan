#ifndef PERSON_H
#define PERSON_H

#include "Mammal.h"

class Person : public Mammal
{
public:
    Person(int breasts, int legs, int hands);
    ~Person();
    void run();

private:
    int m_legs;
    int m_hands;
};

#endif // PERSON_H
