#include "Dolphin.h"
#include <iostream>

Dolphin::Dolphin(int breasts, int fins) : Mammal(breasts)
{
    m_fins = fins;
}

Dolphin::~Dolphin()
{

}

void Dolphin::swim()
{
    std::cout << "I can swim.\n";
}
