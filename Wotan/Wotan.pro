TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Mammal.cpp \
    Person.cpp \
    Dolphin.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    Mammal.h \
    Person.h \
    Dolphin.h

