#ifndef MAMMAL_H
#define MAMMAL_H

class Mammal
{
public:
    explicit Mammal(int breasts = 2);
    ~Mammal();
    void breathe();

private:
    int m_breasts;
};

#endif // MAMMAL_H
