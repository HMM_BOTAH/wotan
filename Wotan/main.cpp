#include <iostream>
#include "Mammal.h"
#include "Person.h"
#include "Dolphin.h"

void func(Mammal & m)
{
    m.breathe();
}

int main()
{
    Mammal some_mammal;
    Person Lada(2,2,2);
    Dolphin Vera(2,4);

    func(some_mammal);
    func(Lada);
    func(Vera);

    return 0;
}
